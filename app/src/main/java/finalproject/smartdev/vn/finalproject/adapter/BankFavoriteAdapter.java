package finalproject.smartdev.vn.finalproject.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import finalproject.smartdev.vn.finalproject.R;
import finalproject.smartdev.vn.finalproject.constants.Constants;
import finalproject.smartdev.vn.finalproject.model.Bank;
import finalproject.smartdev.vn.finalproject.webservice.RestClient;
import finalproject.smartdev.vn.finalproject.webservice.RestResponseHandler;

/**
 * Created by haubv on 21/04/2015.
 */
public class BankFavoriteAdapter extends BaseAdapter implements Filterable {


    protected LayoutInflater layoutInflater;

    protected Context context;

    protected ArrayList<Bank> arrayListBanksOriginal;

    /**
     * The filtered list.
     */
    protected ArrayList<Bank> arrayListBanksFilteredList;

    private SharedPreferences prefs;

    public BankFavoriteAdapter(Context context, ArrayList<Bank> arrayListBanks) {
        this.arrayListBanksOriginal = arrayListBanks;
        this.arrayListBanksFilteredList = arrayListBanks;
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return arrayListBanksFilteredList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayListBanksFilteredList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        convertView = layoutInflater.inflate(R.layout.bank_item, null);

        viewHolder = new ViewHolder();
        viewHolder.banksAddress = (TextView) convertView.findViewById(R.id.bankAddress);
        viewHolder.favariteButton = (ImageView) convertView.findViewById(R.id.favariteButton);


        Bank bank = (Bank) getItem(position);

//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Bank bankChose = (Bank)getItem(position);
//                Toast.makeText(context, bankChose.getAddressBank() + bankChose.getTypeBank(), Toast.LENGTH_SHORT).show();
//            }
//        });

        viewHolder.favariteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bank bankChose = (Bank) getItem(position);
                prefs = context.getSharedPreferences(Constants.NAME_SHARED_PREFENCES, context.MODE_PRIVATE);
                String tokenUser = prefs.getString(Constants.TOKEN, "");
                RestClient.removeBankFromFavariteBanksOfUser(context, tokenUser, bankChose.getId(), new RestResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, String response) {
                        Toast.makeText(context, "Bank was removed from your favorite", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int statusCode, String response) {

                    }
                });
                arrayListBanksFilteredList.remove(bankChose);
                notifyDataSetChanged();
            }
        });

        viewHolder.banksAddress.setText(bank.getTypeBank() + " - " + bank.getAddressBank());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                // TODO Auto-generated method stub
                arrayListBanksFilteredList = (ArrayList<Bank>) results.values;
                notifyDataSetChanged();

            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                if (constraint == null || constraint.length() == 0) {
                    results.values = arrayListBanksOriginal;
                    results.count = arrayListBanksOriginal.size();
                } else {
                    ArrayList<Bank> filterResult = new ArrayList<Bank>();
                    for (Bank data : arrayListBanksOriginal) {
                        if (data.getTypeBank().toLowerCase()
                                .contains(constraint.toString().toLowerCase()) || data.getAddressBank().toLowerCase()
                                .contains(constraint.toString().toLowerCase())) {
                            filterResult.add(data);
                        }
                    }
                    results.values = filterResult;
                    results.count = filterResult.size();
                }

                return results;
            }
        };
    }

    class ViewHolder {
        private TextView banksAddress;
        private ImageView favariteButton;
    }
}
