package finalproject.smartdev.vn.finalproject.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import finalproject.smartdev.vn.finalproject.R;
import finalproject.smartdev.vn.finalproject.constants.Constants;
import finalproject.smartdev.vn.finalproject.model.Bank;
import finalproject.smartdev.vn.finalproject.webservice.RestClient;
import finalproject.smartdev.vn.finalproject.webservice.RestResponseHandler;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, ArrayList<Bank>> _listDataChild;

    private SharedPreferences prefs;

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, ArrayList<Bank>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Bank getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final Bank bankChose = getChild(groupPosition, childPosition);

        final String childText = bankChose.getAddressBank();

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.bank_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.bankAddress);

        ImageView favariteButton = (ImageView) convertView.findViewById(R.id.favariteButton);

        txtListChild.setText(childText);

        favariteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Iterator<Map.Entry<String, ArrayList<Bank>>> iter = _listDataChild.entrySet().iterator();
                while (iter.hasNext()) {
                    Map.Entry<String, ArrayList<Bank>> entry = iter.next();

                    if (entry.getKey().equals(getGroup(groupPosition))) {
                        ArrayList<Bank> forCheck = entry.getValue();
                        for (int i = 0; i < forCheck.size(); i++) {
                            if (forCheck.get(i).equals(bankChose)) {
                                forCheck.remove(bankChose);
                                break;
                            }

                        }
                        entry.setValue(forCheck);
                        break;
                    }

                }
                notifyDataSetChanged();
                prefs = _context.getSharedPreferences(Constants.NAME_SHARED_PREFENCES, _context.MODE_PRIVATE);
                String tokenUser = prefs.getString(Constants.TOKEN, "");
                RestClient.removeBankFromFavariteBanksOfUser(_context, tokenUser, bankChose.getId(), new RestResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, String response) {
                        Toast.makeText(_context, "Bank was removed from your favorite", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int statusCode, String response) {

                    }
                });
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        TextView lblListHeaderNumber = (TextView) convertView
                .findViewById(R.id.lbListHeaderNumber);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        lblListHeaderNumber.setText("(" + getChildrenCount(groupPosition) + ")");

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
