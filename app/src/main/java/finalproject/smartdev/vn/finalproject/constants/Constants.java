package finalproject.smartdev.vn.finalproject.constants;

/**
 * Created by HUNG-HOANG on 4/18/2015.
 */
public final class Constants {
    public static final String LOCALE_ENGLISH_LANGUAGE = "en_US";
    public static final String NAME_SHARED_PREFENCES = "finalproject.smartdev.vn.finalproject";
    public static final String LOCALE_LANGUAGE = "locale_language";
    public static final String MENU_LEFT_PROFILE = "Profile";
    public static final String MENU_LEFT_FAVORITE = "Favorite";
    public static final String MENU_LEFT_EXIT = "Exit";
    public static final String TWITTER_CONSUMER_KEY = "s6bSevhCotlG23O1x2rThEctC";
    public static final String TWITTER_CONSUMER_SECRET_KEY = "4HVO0sFT31UZ8bTAgb0AH47Zc9aBbKjjlG41kBTaUUhEfNWbe2";
    public static final String TWITTER_CALLBACK = "https://smartdev.vn/dragon";
    public static final String TWITTER_OAUTH_VERIFIER = "oauth_verifier";
    public static final String PREF_KEY_TWITTER_LOGIN = "is_twitter_logged_in";
    public static final String DEFAULT_KEY_VALUE = "";
    public static final String USER_NAME = "username";
    public static final String TOKEN = "token";
    public static final String LOGIN_FACEBOOK = "login_facebook";
    public static final String LOGIN_GOOGLE = "login_google";
    public static final String LOGIN_TWITTER = "login_twitter";
    public static final String TOKEN_FACEBOOK = "token_facebook";
    public static final String PREF_TWITTER_OAUTH_KEY = "pref_twitter_oauth_key";
    public static final String PREF_TWITTER_OAUTH_KEY_SECRET = "pref_twitter_oauth_key_secret";
    public static final String FB_EMAIL = "email";
    public static final String FB_BIRTHDAY = "birthday";
    public static final String FB_FIRST_NAME = "first_name";
    public static final String FB_LAST_NAME = "last_name";
    public static final String FB_GENDER = "gender";
    public static final String FB_ID = "id";
    public static final String TWITTER_URL_LOGOUT = "https://api.twitter.com/oauth/authorize?force_login=true";
    public static final String PARAMETER_BANK = "bank";
    public static final String URL_MAP_POSITION = "http://maps.google.com/maps?q=";








}
