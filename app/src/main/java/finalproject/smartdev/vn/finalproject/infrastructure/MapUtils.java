package finalproject.smartdev.vn.finalproject.infrastructure;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import finalproject.smartdev.vn.finalproject.model.Bank;

/**
 * Created by root on 22/04/2015.
 */
public class MapUtils {
    public static double calculationByDistance(LatLng startP, LatLng endP) {
        float[] results = new float[1];
        Location.distanceBetween(startP.latitude, startP.longitude, endP.latitude, endP.longitude, results);
        Log.d("How far?", "Distance: " + String.valueOf(results[0]));
        return results[0];
    }

    public static ArrayList<Bank> getBanksInDistance(int meter, ArrayList<Bank> banks, LatLng location) {
        ArrayList<Bank> result = new ArrayList<>();
        for (Bank bank:banks) {
           double distance = calculationByDistance(location, new LatLng(bank.getLatBank(), bank.getLngBank()));
            Log.d("Distance", "Bank:" + bank.getAddressBank() + " - Distance:" + distance);
           if (distance <= meter) {
               result.add(bank);
           }
        }
        return result;
    }

    private static String getMapsApiDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }


    public static void drawPath(LatLng startL, Bank bank, GoogleMap map, ProgressDialog dialog, HashMap<Marker, Bank> markerBankHashMap, boolean showInfo) {
        if (map != null) {
            clearMap(markerBankHashMap);
            map.clear();
            showATMsOnMap(startL, bank, map, markerBankHashMap, showInfo);
            String url = getMapsApiDirectionsUrl(startL, new LatLng(bank.getLatBank(), bank.getLngBank()));
            ReadTask downloadTask = new ReadTask(map, dialog);
            downloadTask.execute(url);
        }
    }

    public static void showATMsOnMap(int distance, LatLng currentLocation, ArrayList<Bank> banks, GoogleMap map, HashMap<Marker, Bank> markerBankHashMap, boolean showInfo) {
        ArrayList<Bank> nearBanks = new ArrayList<>();
        if (currentLocation == null) {
            return;
        }
        nearBanks = getBanksInDistance(distance, banks, currentLocation);
        showATMsOnMap(currentLocation, nearBanks, map, markerBankHashMap, showInfo);
    }

    public static void showATMsOnMap(LatLng currentLocation, Bank bank, GoogleMap map, HashMap<Marker, Bank> markerBankHashMap, boolean showInfo) {
        ArrayList<Bank> banks = new ArrayList<>();
        banks.add(bank);
        showATMsOnMap(currentLocation, banks, map, markerBankHashMap, showInfo);
    }

    public static void showATMsOnMap(LatLng currentLocation, ArrayList<Bank> banks, GoogleMap map, HashMap<Marker, Bank> markerBankHashMap, boolean showInfo) {
        clearMap(markerBankHashMap);
        map.clear();
        HashMap<Marker, Bank> result = new HashMap<>();
        if (map != null && currentLocation != null && banks != null && banks.size() > 0) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Bank bank : banks) {
                Marker marker = map.addMarker(new MarkerOptions().position(new LatLng(bank.getLatBank(), bank.getLngBank())).title(bank.getTypeBank()).snippet(bank.getAddressBank()));
                if (showInfo) {
                    marker.showInfoWindow();
                }
                bank.setMarker(marker);
                result.put(marker, bank);
                builder.include(new LatLng(bank.getLatBank(), bank.getLngBank()));
            }
            builder.include(currentLocation);
            LatLngBounds bounds = builder.build();
            int padding = 100; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            map.animateCamera(cu);
        }
        addToMarkerBankData(result, markerBankHashMap);
    }

    private static void clearMap(HashMap<Marker, Bank> markerBankHashMap) {
        if (markerBankHashMap != null) {
            for (Marker marker : markerBankHashMap.keySet()) {
                if (marker != null) {
                    marker.remove();
                }
            }
        }
    }

    private static class ReadTask extends AsyncTask<String, Void, String> {
        private GoogleMap map;
        private ProgressDialog progressDialog;

        public ReadTask(GoogleMap map, ProgressDialog progressDialog) {
            this.map = map;
            this.progressDialog = progressDialog;
        }

        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HTTPUtils http = new HTTPUtils();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask(this.map, this.progressDialog).execute(result);
        }
    }

    private static class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        private GoogleMap map;
        private ProgressDialog progressDialog;

        public ParserTask(GoogleMap map, ProgressDialog progressDialog) {
            this.map = map;
            this.progressDialog = progressDialog;
        }

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;

            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<LatLng>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(10);
                polyLineOptions.geodesic(true);
                polyLineOptions.color(Color.RED);
            }
            if (this.map != null) {
                this.map.addPolyline(polyLineOptions);
            }

            if (this.progressDialog != null && this.progressDialog.isShowing()) {
                this.progressDialog.dismiss();
            }
        }
    }

    private static void addToMarkerBankData(HashMap<Marker, Bank> markerBankHashMap, HashMap<Marker, Bank> currentData) {
        if (markerBankHashMap != null) {
            for (Map.Entry entry : markerBankHashMap.entrySet()) {
                if (entry != null && entry.getKey() != null && entry.getValue() != null) {
                    currentData.put((Marker) entry.getKey(), (Bank) entry.getValue());
                }
            }
        }
    }
}
