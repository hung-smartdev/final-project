package finalproject.smartdev.vn.finalproject.infrastructure;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import finalproject.smartdev.vn.finalproject.R;
import finalproject.smartdev.vn.finalproject.constants.Constants;
import finalproject.smartdev.vn.finalproject.view.BaseFragment;
import finalproject.smartdev.vn.finalproject.view.CreateNewAccount;
import finalproject.smartdev.vn.finalproject.view.FavoriteFragment;
import finalproject.smartdev.vn.finalproject.view.ForgotPasswordFragment;
import finalproject.smartdev.vn.finalproject.view.HomeFragment;
import finalproject.smartdev.vn.finalproject.view.LoginFragment;
import finalproject.smartdev.vn.finalproject.view.MapFragment;
import finalproject.smartdev.vn.finalproject.view.ProfileFragment;
import finalproject.smartdev.vn.finalproject.view.ShareFragment;

/**
 * Created by HUNG-HOANG on 4/18/2015.
 */
public class Utils {
    public static void updateLocaleConfiguration(Context context) {
        changeLocaleConfiguration(context, Locale.US);
    }

    public static void changeLocaleConfiguration(Context context,
                                                 Locale newLocale) {
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.locale = newLocale;
        res.updateConfiguration(conf, dm);
    }
    public static String getLocaleDefault(Context context) {
        return Constants.LOCALE_ENGLISH_LANGUAGE;
    }
    public static void saveStringPreferenceWithKey(Context context, String key, String defValue) {
        SharedPreferences.Editor editor = context.getSharedPreferences(
                Constants.NAME_SHARED_PREFENCES, 0).edit();
        editor.putString(key, defValue);
        editor.commit();
    }

    public static void saveBooleanPreferenceWithKey(Context context, String key, boolean defValue) {
        SharedPreferences.Editor editor = context.getSharedPreferences(
                Constants.NAME_SHARED_PREFENCES, 0).edit();
        editor.putBoolean(key, defValue);
        editor.commit();
    }

    public static void exitApp(Activity sInstance) {
        int processId = android.os.Process.myPid();
        sInstance.moveTaskToBack(true);
        sInstance.finish();
        sInstance = null;
        android.os.Process.killProcess(processId);
    }
    public static void showMessageDialog(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.text_title_message));
        builder.setMessage(message);
        builder.setPositiveButton(context.getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                        dialog.dismiss();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void addFragmentAvoidDuplicate(int rootLayout, FragmentManager fragmentManager,
                                                 Fragment fragment, String tag, Bundle bundle) {
        try {
            fragmentManager.executePendingTransactions();
            if (fragmentManager.findFragmentByTag(tag) == null) {
                Log.d("addFragment", tag + "Fragment null");
            }

            FragmentTransaction transaction ;
            Fragment frag = fragmentManager.findFragmentByTag(tag);
            if(frag !=null && frag.isAdded())
            {
                Log.i("addFragment","Remove");
                transaction = fragmentManager.beginTransaction();
                transaction.remove(frag);
                transaction.commit();
                frag = null;
            }

            fragmentManager.executePendingTransactions();
            if (frag == null
                    || !frag.isAdded()) {
                if (bundle != null) {
                    fragment.setArguments(bundle);
                }
                Log.i("addFragment","ADD="+tag);
                transaction = fragmentManager.beginTransaction();
                transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                transaction.add(rootLayout, fragment, tag);
//                transaction.addToBackStack(tag);
                transaction.commit();

            }

        } catch (java.lang.IllegalStateException e) {

        }
    }



    public static void onBackPressFragment(BaseFragment fragment, FragmentManager fragmentManager)
    {
        android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.remove(fragment);
        transaction.commit();
    }

    public static void navigationLoginFragment(FragmentManager fragmentManager,Bundle bundle)
    {
        /*LoginFragment loginFragment = LoginFragment.getInstance();
        Utils.addFragmentAvoidDuplicate(R.id.root_layout,
                fragmentManager, loginFragment,
                LoginFragment.TAG, bundle);*/
    }



    public static void navigationHomeFragment(FragmentManager fragmentManager,Bundle bundle)
    {
        HomeFragment homeFragment = HomeFragment.getInstance();
        Utils.addFragmentAvoidDuplicate(R.id.root_layout,
                fragmentManager, homeFragment,
                HomeFragment.TAG, bundle);
    }

    public static void navigateToMapFragment(FragmentManager fragmentManager, Bundle bundle) {
        MapFragment mapFragment = MapFragment.getInstance();
        Utils.addFragmentAvoidDuplicate(R.id.root_layout,
                fragmentManager, mapFragment, MapFragment.TAG, bundle);
    }

    public static void navigationProfileFragment(FragmentManager fragmentManager,Bundle bundle)
    {
        ProfileFragment profileFragment = ProfileFragment.getInstance();
        Utils.addFragmentAvoidDuplicate(R.id.root_layout,
                fragmentManager, profileFragment,
                ProfileFragment.TAG, bundle);
    }

    public static void navigationFavoriteFragment(FragmentManager fragmentManager,Bundle bundle)
    {
        FavoriteFragment favoriteFragment = FavoriteFragment.getInstance();
        Utils.addFragmentAvoidDuplicate(R.id.root_layout,
                fragmentManager, favoriteFragment,
                FavoriteFragment.TAG, bundle);
    }

    public static String getStringPreferenceWithKey(Context context, String key, String defValue) {
        SharedPreferences preference = context.getSharedPreferences(
                Constants.NAME_SHARED_PREFENCES, 0);
        return preference.getString(key, defValue);
    }

    public static boolean getBooleanPreferenceWithKey(Context context, String key, boolean defValue) {
        SharedPreferences preference = context.getSharedPreferences(
                Constants.NAME_SHARED_PREFENCES, 0);
        return preference.getBoolean(key, defValue);
    }

    public static void navigationCreateNewAccountFragment(FragmentManager fragmentManager,Bundle bundle)
    {
        CreateNewAccount profileFragment = CreateNewAccount.getInstance();
        Utils.addFragmentAvoidDuplicate(R.id.root_layout,
                fragmentManager, profileFragment,
                CreateNewAccount.TAG, bundle);
    }


    public static void navigationForgotPasswordFragment(FragmentManager fragmentManager, Bundle bundle) {
        ForgotPasswordFragment forgotPasswordFragment = ForgotPasswordFragment.getInstance();
        Utils.addFragmentAvoidDuplicate(R.id.root_layout,
                fragmentManager, forgotPasswordFragment,
                ForgotPasswordFragment.TAG, bundle);
    }

    public static void navigationMapsFragment(FragmentManager fragmentManager, Bundle bundle) {
        MapFragment forgotPasswordFragment = MapFragment.getInstance();
        Utils.addFragmentAvoidDuplicate(R.id.root_layout,
                fragmentManager, forgotPasswordFragment,
                MapFragment.TAG, bundle);
    }

    public static void navigationShareFragment(FragmentManager fragmentManager, Bundle bundle) {
        ShareFragment shareFragment = ShareFragment.getInstance();
        Utils.addFragmentAvoidDuplicate(R.id.root_layout,
                fragmentManager, shareFragment,
                ShareFragment.TAG, bundle);
    }

    public static boolean edtIsEmpty(EditText edt){
        if(edt.getText() != null && !edt.getText().toString().equals("")){
            return false;
        }
        return true;
    }

    public static boolean compairPassword(String password, String comfirm){
        if(password.equals(comfirm)){
            return true;
        }
        return false;
    }

    public static boolean validUsername(String string){
        return string.matches("^[a-zA-Z0-9_]{3,15}$");
    }

    public static boolean validName(String string){
        return string.matches("^[a-zA-Z0-9 ]{3,15}$");
    }

    public static boolean validEmail(String string){
        Pattern regex = Pattern.compile("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$");
        Matcher matcher = regex .matcher(string);
        return matcher.matches();
    }

    public static boolean validPassword(String string){
        //Pattern regex = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{6,}$");
        Pattern regex = Pattern.compile("^[a-zA-Z0-9]{6,15}$");
        Matcher matcher = regex .matcher(string);
        return matcher.matches();
    }

    public static void showDialog(final Activity context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(context.getResources().getString(R.string.text_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                        dialog.dismiss();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void showDialogToken(final Activity context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Log Out",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                        dialog.dismiss();
                        Utils.saveStringPreferenceWithKey(context, Constants.USER_NAME, null);
                        Utils.saveStringPreferenceWithKey(context, Constants.TOKEN, null);
                        context.onBackPressed();

                        Intent intent = new Intent(context, LoginFragment.class);
                        context.startActivity(intent);
                        context.finish();
                    }
                });
        final AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void setDateTimeField(final Activity context, final EditText editText) {

        int day, month, year = 0;
        Calendar newCalendar = Calendar.getInstance();

        if(null != editText.getText().toString() && editText.getText().toString().contains("-")){
            day = Integer.parseInt(editText.getText().toString().split("-")[0]);
            month = Integer.parseInt(editText.getText().toString().split("-")[1]);
            year = Integer.parseInt(editText.getText().toString().split("-")[2]);
        }else{
            day = newCalendar.DAY_OF_MONTH;
            month = newCalendar.MONTH;
            year = newCalendar.YEAR;
        }
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                editText.setText(dateFormatter.format(newDate.getTime()));
            }

        },year, month, day);
        datePickerDialog.show();
        datePickerDialog.setCancelable(false);
    }

    public static Fragment getCurrentFragment(FragmentManager fragmentManager){


        String fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        Fragment currentFragment = fragmentManager
                .findFragmentByTag(fragmentTag);
        return currentFragment;
    }
}
