package finalproject.smartdev.vn.finalproject.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.Marker;

/**
 * Created by Hoang on 4/20/2015.
 */
public class Bank implements Parcelable {
    private String id;
    private String typeBank;
    private String addressBank;
    private float latBank;
    private float lngBank;
    private Marker marker;

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    private Boolean freeCharge;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypeBank() {
        return typeBank;
    }

    public void setTypeBank(String typeBank) {
        this.typeBank = typeBank;
    }

    public String getAddressBank() {
        return addressBank;
    }

    public void setAddressBank(String addressBank) {
        this.addressBank = addressBank;
    }

    public float getLatBank() {
        return latBank;
    }

    public void setLatBank(float latBank) {
        this.latBank = latBank;
    }

    public float getLngBank() {
        return lngBank;
    }

    public void setLngBank(float ingBank) {
        this.lngBank = ingBank;
    }

    public Boolean getFreeCharge() {
        return freeCharge;
    }

    public void setFreeCharge(Boolean freeCharge) {
        this.freeCharge = freeCharge;
    }

    @Override
    public boolean equals(Object o) {
        if (getAddressBank().equals(((Bank) o).getAddressBank()) && getId().equals(((Bank) o).getId()) && getTypeBank().equals(((Bank) o).getTypeBank())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(typeBank);
        dest.writeString(addressBank);
        dest.writeFloat(latBank);
        dest.writeFloat(lngBank);

    }

    public static final Parcelable.Creator<Bank> CREATOR = new Creator<Bank>() {
        @Override
        public Bank createFromParcel(Parcel source) {

                Bank bank = new Bank();
                bank.id = source.readString();
                bank.typeBank = source.readString();
                bank.addressBank = source.readString();
                bank.latBank = source.readFloat();
                bank.lngBank = source.readFloat();
                return bank;
        }

        @Override
        public Bank[] newArray(int size) {
            return new Bank[0];
        }
    };
}
