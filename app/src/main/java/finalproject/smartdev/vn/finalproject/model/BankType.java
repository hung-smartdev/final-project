package finalproject.smartdev.vn.finalproject.model;

/**
 * Created by haubv on 21/04/2015.
 */
public class BankType {
    public static final String HSBC = "HSBC";
    public static final String TEACHCOMBANK = "TechComBank";
    public static final String VIETCOMBANK = "VietComBank";
    public static final String DONGA = "DongABank";
}
