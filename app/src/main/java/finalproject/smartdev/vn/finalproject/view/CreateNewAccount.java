package finalproject.smartdev.vn.finalproject.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import finalproject.smartdev.vn.finalproject.R;
import finalproject.smartdev.vn.finalproject.constants.Constants;
import finalproject.smartdev.vn.finalproject.infrastructure.Utils;
import finalproject.smartdev.vn.finalproject.model.Users;
import finalproject.smartdev.vn.finalproject.webservice.RestClient;
import finalproject.smartdev.vn.finalproject.webservice.RestResponseHandler;

/**
 * Created by longph on 4/21/2015.
 */
public class CreateNewAccount extends BaseFragment implements View.OnClickListener{

    public static String TAG = "CreateNewAccount";
    private EditText edtFirstname, edtLastname, edtEmail, edtUsername, edtAge, edtPassword, edtConfirm;
    private Spinner spGender;
    private Button btnCreate, btnCancel;
    private Context mContext;
    private static CreateNewAccount instance;

    public static CreateNewAccount getInstance(){
        if(instance == null){
            instance = new CreateNewAccount();
        }
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frament_create_new_user, null);
        //mContext = this;
        initViews(view);
        addListenerForViews();
        bindDataOnViews();
        return view;
    }

    @Override
    protected void initViews(View view) {
        edtFirstname = (EditText) view.findViewById(R.id.edtFirstname);
        edtLastname = (EditText) view.findViewById(R.id.edtLastname);
        edtEmail = (EditText) view.findViewById(R.id.edtEmail);
        edtUsername = (EditText) view.findViewById(R.id.edtUsername);
        edtAge = (EditText) view.findViewById(R.id.edtAge);
        edtPassword = (EditText) view.findViewById(R.id.edtPassword);
        edtConfirm = (EditText) view.findViewById(R.id.edtConfirmpassword);
        spGender = (Spinner) view.findViewById(R.id.spGender);
        btnCreate = (Button) view.findViewById(R.id.btnSubmit);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
    }

    @Override
    protected void addListenerForViews() {
        btnCreate.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        edtAge.setOnClickListener(this);
    }

    @Override
    protected void bindDataOnViews() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.array_gender, R.layout.custom_spinner_item);
        spGender.setAdapter(adapter);
    }

    public void checkEdtIsValid(){
        if(Utils.edtIsEmpty(edtFirstname) || !Utils.validName(edtFirstname.getText().toString())){
            edtFirstname.setError("Frist Name is not valid !");
        }else if(Utils.edtIsEmpty(edtLastname) || !Utils.validName(edtLastname.getText().toString())){
            edtLastname.setError("Last Name is not valid !");
        }else if(Utils.edtIsEmpty(edtEmail) || !Utils.validEmail(edtEmail.getText().toString())){
            edtEmail.setError("Email is not valid !");
        }else if(Utils.edtIsEmpty(edtUsername) || !Utils.validUsername(edtUsername.getText().toString())){
            edtUsername.setError("User Name is not valid !");
        }else if(Utils.edtIsEmpty(edtAge)){
            edtAge.setError("Age must not empty !");
        }else if(Utils.edtIsEmpty(edtPassword) || !Utils.validPassword(edtPassword.getText().toString())){
            edtPassword.setError("Password is not valid !");
        }else if(Utils.edtIsEmpty(edtConfirm) || !Utils.validPassword(edtConfirm.getText().toString())){
            edtConfirm.setError("Confirm password is not valid !");
        }else if(!Utils.compairPassword(edtPassword.getText().toString(), edtConfirm.getText().toString())){
            Toast.makeText(getActivity().getApplicationContext(), "Password not matches !", Toast.LENGTH_SHORT).show();
        }else{
            createUser();
        }
    }

    public void createUser(){
        Users users = new Users();
        users.setFirstName(edtFirstname.getText().toString());
        users.setLastName(edtLastname.getText().toString());
        users.setUserName(edtUsername.getText().toString());
        users.setAge(edtAge.getText().toString());
        users.setEmail(edtEmail.getText().toString());
        users.setGender(spGender.getSelectedItem().toString());
        users.setPassword(edtPassword.getText().toString());
        RestClient.createNewAccount(getActivity().getApplicationContext(), users, new RestResponseHandler() {
            @Override
            public void onSuccess(int statusCode, String response) {
                Log.d("check string: ", response.toString());
                try {
                    if(statusCode == 200) {
                        JSONObject json = new JSONObject(response);
                        Utils.saveStringPreferenceWithKey(getActivity().getApplicationContext(), Constants.USER_NAME, json.getString("userName"));
                        Toast.makeText(getActivity().getApplicationContext(), "Register Successfully !!", Toast.LENGTH_SHORT).show();
                        try {
                            Thread.sleep(4000);
                            onBackPress();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }else{
                        JSONObject json = new JSONObject(response);
                        Utils.showDialog(getActivity(), "Failure !!", json.getString("error"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Utils.showDialog(getActivity(), "Failure !!", json.getString("error"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSubmit:
                checkEdtIsValid();
                break;
            case R.id.btnCancel:
                onBackPress();
                break;
            case R.id.edtAge:
                Utils.setDateTimeField(getActivity(), edtAge);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onBackPress() {
        super.onBackPress();
        instance=null;
    }
    /*
    public void onBackPress() {
        CreateNewAccount createNewAccount = CreateNewAccount.getInstance();
        android.support.v4.app.FragmentTransaction transaction = getActivity()
                .getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.remove(createNewAccount);
        transaction.commit();
    }*/
}
