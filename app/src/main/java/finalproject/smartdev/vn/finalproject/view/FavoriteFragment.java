package finalproject.smartdev.vn.finalproject.view;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import finalproject.smartdev.vn.finalproject.R;
import finalproject.smartdev.vn.finalproject.adapter.BankFavoriteAdapter;
import finalproject.smartdev.vn.finalproject.adapter.ExpandableListAdapter;
import finalproject.smartdev.vn.finalproject.constants.Constants;
import finalproject.smartdev.vn.finalproject.infrastructure.Utils;
import finalproject.smartdev.vn.finalproject.model.Bank;
import finalproject.smartdev.vn.finalproject.model.BankType;
import finalproject.smartdev.vn.finalproject.webservice.RestClient;
import finalproject.smartdev.vn.finalproject.webservice.RestResponseHandler;

/**
 * Created by HUNG-HOANG on 4/18/2015.
 */
public class FavoriteFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = "FavoriteFragment";

    private static FavoriteFragment instance;

    public static FavoriteFragment getInstance() {
        if (instance == null) {
            instance = new FavoriteFragment();
        }
        return instance;
    }

    private ArrayList<Bank> favoriteBanks = new ArrayList<Bank>();

    private ArrayList<Bank> favoriteBanksHSBC = new ArrayList<Bank>();

    private ArrayList<Bank> favoriteBanksTeachcombank = new ArrayList<Bank>();

    private ArrayList<Bank> favoriteBanksVietcombank = new ArrayList<Bank>();

    private ArrayList<Bank> favoriteBanksDongA = new ArrayList<Bank>();

    private int heightOfListview = 200;

    private boolean mMeasured = false;

    private ScrollView favoriteScroll;

    private View.OnTouchListener disableScrollLitener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Log.i("hau", "Touched...");
            return true;
        }
    };

    private ExpandableListAdapter listAdapter;

    private ExpandableListView expListView;

    private List<String> listDataHeader;

    private HashMap<String, ArrayList<Bank>> listDataChild;

    private SharedPreferences prefs;

    private ArrayList<ArrayList<Bank>> arrayListsAllBank;

    private ListView listBankFiltered;

    private BankFavoriteAdapter bankFavoriteAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_favorite, null);

        initViews(view);
        addListenerForViews();

        prefs = getActivity().getSharedPreferences(Constants.NAME_SHARED_PREFENCES, getActivity().MODE_PRIVATE);
        String tokenUser = prefs.getString(Constants.TOKEN, "");
        Log.i("token", tokenUser);
        RestClient.getAllFavariteBanksOfUser(getActivity(), tokenUser, new RestResponseHandler() {
            @Override
            public void onSuccess(int statusCode, String response) {
                Gson gson = new GsonBuilder().create();
                Log.i("favorite respone", response);
                if (favoriteBanks.size() > 0) {
                    favoriteBanks.clear();
                }

                clearOldData();
                favoriteBanks.addAll(Arrays.asList(gson.fromJson(response, Bank[].class)));

                prepareListData(favoriteBanks);

                listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);

                // setting list adapter
                expListView.setAdapter(listAdapter);
                bankFavoriteAdapter.notifyDataSetChanged();
                listBankFiltered.setAdapter(bankFavoriteAdapter);
            }

            @Override
            public void onFailure(int statusCode, String response) {
                if (statusCode == 401) {
                    Utils.showDialogToken(getActivity(), "Failure !!", "Your account has been logged in by another device. Please try to login again !");
                }
            }
        });

        return view;
    }

    /*
     * Preparing the list data
	 */
    public void prepareListData(ArrayList<Bank> favoriteBanks) {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, ArrayList<Bank>>();

        for (int i = 0; i < favoriteBanks.size(); i++) {
            listDataHeader.add(favoriteBanks.get(i).getTypeBank());
        }

        Set<String> set = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
        set.addAll(listDataHeader);
        listDataHeader = new ArrayList<String>(set);

        arrayListsAllBank = new ArrayList<ArrayList<Bank>>(listDataHeader.size());
        arrayListsAllBank.add(favoriteBanksHSBC);
        arrayListsAllBank.add(favoriteBanksVietcombank);
        arrayListsAllBank.add(favoriteBanksDongA);
        arrayListsAllBank.add(favoriteBanksTeachcombank);

        for (int i = 0; i < listDataHeader.size(); i++) {
            Log.i("listDataHeader", listDataHeader.get(i));
            for (int j = 0; j < favoriteBanks.size(); j++) {
                Log.i("listDataHeader", favoriteBanks.get(j).getTypeBank());
                if (favoriteBanks.get(j).getTypeBank().equals(listDataHeader.get(i))) {
                    arrayListsAllBank.get(i).add(favoriteBanks.get(j));
                }
            }
        }


        for (int i = 0; i < listDataHeader.size(); i++) {
            listDataChild.put(listDataHeader.get(i), arrayListsAllBank.get(i));
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        bankFavoriteAdapter = new BankFavoriteAdapter(getActivity(), favoriteBanks);
        //super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_favorite, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        final android.support.v7.widget.SearchView searchView = (android.support.v7.widget.SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                bankFavoriteAdapter.getFilter().filter(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.i("searchView", searchView.getQuery().toString());
                bankFavoriteAdapter.getFilter().filter(s);
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                expListView.setVisibility(View.VISIBLE);
                listBankFiltered.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                expListView.setVisibility(View.GONE);
                listBankFiltered.setVisibility(View.VISIBLE);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            /** EDIT **/
            case R.id.action_search:
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }//end switch
    }

    @Override
    protected void initViews(View view) {

        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);
        listBankFiltered = (ListView) view.findViewById(R.id.listBank);

    }


    @Override
    protected void addListenerForViews() {

        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
                Toast.makeText(
                        getActivity(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + listDataChild.get(
                                listDataHeader.get(groupPosition)).get(
                                childPosition).getAddressBank(), Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        });
    }

    @Override
    protected void bindDataOnViews() {
        clearOldData();
        if (favoriteBanks != null && favoriteBanks.size() > 0) {
            for (int i = 0; i < favoriteBanks.size(); i++) {
                Bank bank = favoriteBanks.get(i);
                if (bank.getTypeBank().equalsIgnoreCase(BankType.TEACHCOMBANK)) {
                    favoriteBanksTeachcombank.add(bank);
                } else if (bank.getTypeBank().equalsIgnoreCase(BankType.DONGA)) {
                    favoriteBanksDongA.add(bank);
                } else if (bank.getTypeBank().equalsIgnoreCase(BankType.HSBC)) {
                    favoriteBanksHSBC.add(bank);
                } else if (bank.getTypeBank().equalsIgnoreCase(BankType.VIETCOMBANK)) {
                    favoriteBanksVietcombank.add(bank);
                }
            }
        } else {
            return;
        }


    }

    private void clearOldData() {
        if (listDataHeader != null && listDataHeader.size() > 0) {
            listDataHeader.clear();
        }
        if (listDataChild != null && listDataChild.size() > 0) {
            listDataChild.clear();
        }
        if (arrayListsAllBank != null && arrayListsAllBank.size() > 0) {
            arrayListsAllBank.clear();
        }
        if (favoriteBanksTeachcombank.size() > 0) {
            favoriteBanksTeachcombank.clear();
        }
        if (favoriteBanksVietcombank.size() > 0) {
            favoriteBanksVietcombank.clear();
        }
        if (favoriteBanksHSBC.size() > 0) {
            favoriteBanksHSBC.clear();
        }
        if (favoriteBanksDongA.size() > 0) {
            favoriteBanksDongA.clear();
        }
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onBackPress() {
        super.onBackPress();
        instance=null;
    }


}


