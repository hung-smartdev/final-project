package finalproject.smartdev.vn.finalproject.view;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import finalproject.smartdev.vn.finalproject.R;
import finalproject.smartdev.vn.finalproject.constants.Constants;
import finalproject.smartdev.vn.finalproject.infrastructure.Utils;
import finalproject.smartdev.vn.finalproject.webservice.RestClient;
import finalproject.smartdev.vn.finalproject.webservice.RestResponseHandler;

/**
 * Created by longph on 4/21/2015.
 */
public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener{

    public static String TAG = "ForgotPasswordFragment";
    private static ForgotPasswordFragment instance;
    private Button btnSubmit, btnCancel;
    private EditText edtForgotpassword;

    public static ForgotPasswordFragment getInstance(){
        if(instance == null){
            instance = new ForgotPasswordFragment();
        }
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frament_forgot_password, null);
        initViews(view);
        addListenerForViews();
        return view;
    }

    @Override
    protected void initViews(View view) {
        btnSubmit = (Button) view.findViewById(R.id.btnSubmit);
        btnCancel = (Button) view.findViewById(R.id.btnCancel2);
        edtForgotpassword = (EditText) view.findViewById(R.id.edtEmail);
    }

    @Override
    protected void addListenerForViews() {
        btnSubmit.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    protected void bindDataOnViews() {

    }

    @Override
    protected void onBackPress() {
        super.onBackPress();
        instance=null;
    }/*
    public void onBackPress() {
        ForgotPasswordFragment forgotPasswordFragment = ForgotPasswordFragment.getInstance();
        android.support.v4.app.FragmentTransaction transaction = getActivity()
                .getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.remove(forgotPasswordFragment);
        transaction.commit();
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSubmit:
                if(Utils.edtIsEmpty(edtForgotpassword) || !Utils.validEmail(edtForgotpassword.getText().toString())){
                    edtForgotpassword.setError("Email is not valid !");
                }else{
                    RestClient.forgotPassword(getActivity().getApplicationContext(), edtForgotpassword.getText().toString(), new RestResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, String response) {
                            Log.d("Console : ", response);
                            if(statusCode == 200){
                                Toast.makeText(getActivity().getApplicationContext(), "Reset password successful !!", Toast.LENGTH_SHORT).show();
                            }else{
                                try {
                                    JSONObject json = new JSONObject(response);
                                    Utils.showDialog(getActivity(), "Failure !!", json.getString("error"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, String response) {
                            Log.d("Console Failure : ", response);
                            try {
                                JSONObject json = new JSONObject(response);
                                Utils.showDialog(getActivity(), "Failure !!", json.getString("error"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                break;
            case R.id.btnCancel2:
                onBackPress();
                break;
            default:
                break;
        }
    }
}
