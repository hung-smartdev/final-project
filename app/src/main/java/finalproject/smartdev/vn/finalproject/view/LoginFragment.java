package finalproject.smartdev.vn.finalproject.view;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import finalproject.smartdev.vn.finalproject.R;
import finalproject.smartdev.vn.finalproject.constants.Constants;
import finalproject.smartdev.vn.finalproject.infrastructure.Utils;
import finalproject.smartdev.vn.finalproject.model.Users;
import finalproject.smartdev.vn.finalproject.webservice.RestClient;
import finalproject.smartdev.vn.finalproject.webservice.RestResponseHandler;
import twitter4j.Twitter;
import twitter4j.User;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by HUNG-HOANG on 4/18/2015.
 */
public class LoginFragment extends ActionBarActivity implements View.OnClickListener,FacebookCallback<LoginResult>
,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GraphRequest.GraphJSONObjectCallback
{
    public static final String TAG = "LoginFragment";
    private static LoginFragment instance;
    private static final int RC_SIGN_IN = 1000;
    private static final List<String> PERMISSIONS_FACE = Arrays.asList("public_profile", "email","user_birthday");
    private LoginButton btnLoginFB;
    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private SignInButton btnLoginGoogle;
    private boolean mIntentInProgress;
    private ConnectionResult mConnectionResult;
    private boolean mSignInClicked;
    private ImageView imgTwitter;
    private static Twitter twitter;
    private static RequestToken requestToken;
    public static final int WEB_VIEW_REQUEST_CODE = 100;
    private Button btnCreateNewAccount, btnForgotPassword, btnLogin;
    private EditText edtUserName, edtPassword;
    private String loginWith = null;

    public static LoginFragment getInstance() {
        if (instance == null) {
            instance = new LoginFragment();
        }
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.fragment_signin);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();

        initViews();
        addListenerForViews();
        bindDataOnViews();
    }

    protected void initViews() {
        btnLoginFB = (LoginButton) findViewById(R.id.btnLoginFB);
        btnLoginFB.setReadPermissions(PERMISSIONS_FACE);
        //btnLoginFB.setFragment();
        btnLoginFB.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);

        btnLoginGoogle = (SignInButton)findViewById(R.id.btnLoginGoogle);
        btnLoginGoogle.setSize(SignInButton.SIZE_ICON_ONLY);

        imgTwitter = (ImageView)findViewById(R.id.imgTwitter);

        btnCreateNewAccount = (Button) findViewById(R.id.btnCreateAccount);
        btnForgotPassword = (Button) findViewById(R.id.btnForgotPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        edtUserName = (EditText) findViewById(R.id.edtUsername);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
    }

    protected void addListenerForViews() {
        btnLoginFB.setOnClickListener(this);
        LoginManager.getInstance().registerCallback(callbackManager,this);

        btnLoginGoogle.setOnClickListener(this);

        imgTwitter.setOnClickListener(this);
        btnCreateNewAccount.setOnClickListener(this);
        btnForgotPassword.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    protected void bindDataOnViews() {
        googleLogout();
        facebookLogout();
        twitterLogout();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLoginFB:
                Log.i(TAG, "click btnLoginFB");
                loginWith = Constants.LOGIN_FACEBOOK;
                break;
            case R.id.btnLoginGoogle:
                Log.i(TAG, "click btnLoginGoogle");
                loginWith = Constants.LOGIN_GOOGLE;
                googleLogin();
                break;
            case R.id.imgTwitter:
                Log.i(TAG, "click imgTwitter");
                loginWith = Constants.LOGIN_TWITTER;
                loginTwitter();
                break;
            case R.id.btnCreateAccount:
                Utils.navigationCreateNewAccountFragment(this.getSupportFragmentManager(), null);
                break;
            case R.id.btnForgotPassword:
                Utils.navigationForgotPasswordFragment(this.getSupportFragmentManager(), null);
                break;
            case R.id.btnLogin:
                if(Utils.edtIsEmpty(edtUserName) || !Utils.validUsername(edtUserName.getText().toString())){
                    edtUserName.setError("User name is not valid !");
                }else if(Utils.edtIsEmpty(edtPassword) || !Utils.validPassword(edtPassword.getText().toString())){
                    edtPassword.setError("Password is not valid !");
                }else {
                    final Users user = new Users();
                    user.setUserName(edtUserName.getText().toString());
                    user.setPassword(edtPassword.getText().toString());
                    RestClient.login(this.getApplicationContext(), user, new RestResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, String response) {
                            if (statusCode == 200) {
                                try {
                                    JSONObject json = new JSONObject(response);
                                    Utils.saveStringPreferenceWithKey(LoginFragment.this, Constants.USER_NAME, json.getString("userName"));
                                    Utils.saveStringPreferenceWithKey(LoginFragment.this, Constants.TOKEN, json.getString("token"));
                                    Intent intent = new Intent(LoginFragment.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                try {
                                    JSONObject json = new JSONObject(response);
                                    Utils.showDialog(LoginFragment.this, "Failure !!", json.getString("error"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, String response) {
                            try {
                                JSONObject json = new JSONObject(response);
                                Utils.showDialog(LoginFragment.this, "Failure !!", json.getString("error"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
                break;
            default:
                break;

        };

    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        Log.i(TAG, "onSuccess FB" + loginResult);
        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),this);
        request.executeAsync();
       // facebookLogout();
    }

    private void resolveSignInError() {
        if (mConnectionResult != null && mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                //mConnectionResult.startResolutionForResult(getActivity(), RC_SIGN_IN);
                this.startIntentSenderForResult(mConnectionResult.getResolution().getIntentSender(), RC_SIGN_IN, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    private void googleLogin()
    {
        mSignInClicked = true;
        mGoogleApiClient.connect();
    }

    private void facebookLogout()
    {
        LoginManager.getInstance().logOut();
    }

    @Override
    public void onCancel() {
        Log.i(TAG,"onCancel FB");
    }

    @Override
    public void onError(FacebookException e) {
        Log.i(TAG,"onError FB");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG,"onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        if(loginWith!=null) {
            if (loginWith.equalsIgnoreCase(Constants.LOGIN_FACEBOOK)) {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            } else if (loginWith.equalsIgnoreCase(Constants.LOGIN_GOOGLE)) {
                if (requestCode == RC_SIGN_IN) {
                    if (resultCode != Activity.RESULT_OK) {
                        mSignInClicked = false;
                    }

                    mIntentInProgress = false;

                    if (!mGoogleApiClient.isConnected()) {
                        mGoogleApiClient.reconnect();
                    }
                }

            } else if (loginWith.equalsIgnoreCase(Constants.LOGIN_TWITTER)) {
                if (resultCode == Activity.RESULT_OK) {
                    String verifier = data.getExtras().getString(Constants.TWITTER_OAUTH_VERIFIER);
                    try {
                           if (twitter != null) {
                            twitter4j.auth.AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
                            getProfileTwitter(accessToken);

                        }
                    } catch (TwitterException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
   }

    public void googleLogout()
    {


        Log.i(TAG, "Start googleLogout");
        if (mGoogleApiClient.isConnected()) {
            Log.i(TAG, "Logout Google 1");
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            btnLoginGoogle.setEnabled(true);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            Log.i(TAG, "Logout Google");
                            mGoogleApiClient.disconnect();
                            // mGoogleApiClient.connect();
                        }
                    });
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.i(TAG, "onConnected Google");
        mSignInClicked = false;
        getProfileGoogle();



    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG,"onConnectionSuspended Google");
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG,"onConnectionFailed Google");
        if (!mIntentInProgress) {
            if (mSignInClicked && result.hasResolution()) {
                try {
                    result.startResolutionForResult(this, RC_SIGN_IN);
                    mIntentInProgress = true;
                } catch (IntentSender.SendIntentException e) {
                    mIntentInProgress = false;
                    mGoogleApiClient.connect();
                }
            }
        }

    }

    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        Log.i(TAG,"onStart Google");

    }

    public void onStop() {
        super.onStop();

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        Log.i(TAG,"onStop Google");
    }

    private void getProfileGoogle()
    {
        if (mGoogleApiClient.isConnected() && Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
          Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
          String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
          Log.i(TAG,"GOOGLE Person="+person.toString()+ "Email:"+email);
          registerAccountGoogle(person);

        }
    }

    private void registerAccountGoogle(Person person){
        if(person !=null)
        {
            Users userPost=new Users();
            userPost.setIsSocial("true");
            userPost.setEmail(Plus.AccountApi.getAccountName(mGoogleApiClient));
            try {
                JSONObject userJSON = new JSONObject(person.toString());
                JSONObject nameJSON = userJSON.getJSONObject("name");
                userPost.setLastName(nameJSON.getString("familyName"));
                userPost.setFirstName(nameJSON.getString("givenName"));
                userPost.setGender(userJSON.getString("gender"));
                userPost.setUserName("g" + Plus.AccountApi.getAccountName(mGoogleApiClient));
                userPost.setToken(Plus.AccountApi.getAccountName(mGoogleApiClient));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            //  userPost.setFirstName(person.get);

            registerSocial(userPost);
        }
    }

    @Override
    public void onCompleted(final JSONObject userJSON, GraphResponse graphResponse) {
        if (userJSON != null) {
            Users userPost=new Users();
                try {

                    userPost.setEmail(userJSON.getString(Constants.FB_EMAIL).toString());

                    if(checkKeyFacebook(Constants. FB_BIRTHDAY,userJSON)) {
                        userPost.setAge(userJSON.getString(Constants. FB_BIRTHDAY).toString());
                    }

                    userPost.setFirstName(userJSON.getString(Constants.FB_FIRST_NAME).toString());
                    userPost.setLastName(userJSON.getString(Constants.FB_LAST_NAME).toString());
                    userPost.setGender(userJSON.getString(Constants.FB_GENDER).toString());
                    userPost.setIsSocial("true");
                    userPost.setToken(AccessToken.getCurrentAccessToken().getToken());

                    if(checkKeyFacebook(Constants.FB_EMAIL,userJSON) && userJSON.getString(Constants.FB_EMAIL) ==null)
                    {
                        userPost.setUserName("fb"+userJSON.getString(Constants.FB_ID).toString());
                    }else{
                        userPost.setUserName("fb"+userJSON.getString(Constants.FB_EMAIL).toString());
                    }
                    registerSocial(userPost);

                } catch (JSONException e) {
                    e.printStackTrace();
                }



            Log.i(TAG,"FaceboobPOST:"+userJSON.toString()+"");
        }
    }

    private boolean checkKeyFacebook(String key, JSONObject data)
    {
        Iterator<String> keys=data.keys();
        String value="";
        while(keys.hasNext())
        {

           value = keys.next();
            if(key.equalsIgnoreCase(value))
            {
                return true;
            }
        }
        return false;
    }

    private void registerSocial(final Users userPost)
    {

        RestClient.createNewAccountSocial(this,userPost,new RestResponseHandler() {
            @Override
            public void onSuccess(int statusCode, String response) {
                Log.i(TAG,"onSuccess POST"+statusCode);
                Log.i(TAG,"FaceboobPOST TOKEN:["+userPost.getToken()+"]|||||");
                navigationHome(userPost.getUserName(),userPost.getToken());
            }

            @Override
            public void onFailure(int statusCode, String response) {
                Log.i(TAG,"onFailure"+statusCode);
                Log.i(TAG,"onFailure Response"+response);
                googleLogout();
                facebookLogout();
                twitterLogout();

                try {

                    JSONObject json = new JSONObject(response);
                    Utils.showDialog(LoginFragment.this, "Failure !!", json.getString("error"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }
    private void twitterLogout()
    {
        String token = Utils.getStringPreferenceWithKey(getApplicationContext(),Constants.PREF_TWITTER_OAUTH_KEY,"");
        Log.i(TAG,"twitterLogout token"+token);
        if(!token.isEmpty()) {
            String urlLogout = Constants.TWITTER_URL_LOGOUT;
            final Intent intent = new Intent(this, WebViewActivity.class);
            intent.putExtra(WebViewActivity.EXTRA_URL, urlLogout);
            startActivityForResult(intent, WEB_VIEW_REQUEST_CODE);
            Utils.saveStringPreferenceWithKey(this, Constants.PREF_TWITTER_OAUTH_KEY, "");
            Utils.saveStringPreferenceWithKey(this, Constants.PREF_TWITTER_OAUTH_KEY_SECRET, "");
            Utils.saveBooleanPreferenceWithKey(this,Constants.PREF_KEY_TWITTER_LOGIN,false);
        }

    }
    private void loginTwitter()
    {
        boolean isLoginIn = Utils.getBooleanPreferenceWithKey(this.getApplicationContext(), Constants.PREF_KEY_TWITTER_LOGIN, false);
        if( !isLoginIn )
        {
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(Constants.TWITTER_CONSUMER_KEY);
            builder.setOAuthConsumerSecret(Constants.TWITTER_CONSUMER_SECRET_KEY);
            Configuration configuration = builder.build();
            TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();
            try {
                requestToken = twitter.getOAuthRequestToken(Constants.TWITTER_CALLBACK);
                Intent intent = new Intent(this, WebViewActivity.class);
                intent.putExtra(WebViewActivity.EXTRA_URL, requestToken.getAuthenticationURL());
                startActivityForResult(intent, WEB_VIEW_REQUEST_CODE);

            } catch (TwitterException e) {
                e.printStackTrace();
            }
        }
    }

    private void getProfileTwitter(twitter4j.auth.AccessToken accessToken){

        long userID = accessToken.getUserId();
        Utils.saveBooleanPreferenceWithKey(this,Constants.PREF_KEY_TWITTER_LOGIN,true);
        Utils.saveStringPreferenceWithKey(this,Constants.PREF_TWITTER_OAUTH_KEY,accessToken.getToken());
        Utils.saveStringPreferenceWithKey(this,Constants.PREF_TWITTER_OAUTH_KEY_SECRET,accessToken.getTokenSecret());
        final User user;
        try {
            user = twitter.showUser(userID);
            Users userPost=new Users();
            userPost.setUserName("t"+user.getScreenName());
            userPost.setToken(accessToken.getToken());
            userPost.setIsSocial("true");
            String fullName=user.getName();
            if(fullName !=null)
            {
                String[] split = fullName.split(" ");
                String lastName="";
                if(split.length>1){
                    userPost.setFirstName(split[0]);
                    for(int i=1;i<split.length;i++){
                      lastName= lastName+ split[i]+" ";
                    }
                    userPost.setLastName(lastName);
                }
                else{
                    userPost.setLastName(fullName);

                }
            }

            registerSocial(userPost);

            Log.i(TAG, "" + twitter.showUser(user.getScreenName()));
            Log.i(TAG, "Twitter:" + user.toString() + "Name:" + user.getName());
        } catch (TwitterException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {

        CreateNewAccount createNewAccount = (CreateNewAccount) getSupportFragmentManager()
                .findFragmentByTag(CreateNewAccount.TAG);

        ForgotPasswordFragment forgotPasswordFragment = (ForgotPasswordFragment) getSupportFragmentManager()
                .findFragmentByTag(ForgotPasswordFragment.TAG);

        if (createNewAccount != null) {
            createNewAccount.onBackPress();
            return;
        }else if (forgotPasswordFragment != null) {
            forgotPasswordFragment.onBackPress();
            return;
        }else{
            finish();
        }
    }

    public void navigationHome(String userName, String token)
    {
        Utils.saveStringPreferenceWithKey(LoginFragment.this, Constants.USER_NAME, userName);
        Utils.saveStringPreferenceWithKey(LoginFragment.this, Constants.TOKEN, token);
        Intent intent = new Intent(LoginFragment.this, MainActivity.class);
        startActivity(intent);
    }
}
