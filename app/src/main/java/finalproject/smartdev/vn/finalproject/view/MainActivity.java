package finalproject.smartdev.vn.finalproject.view;

import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import finalproject.smartdev.vn.finalproject.R;
import finalproject.smartdev.vn.finalproject.adapter.NavDrawerItem;
import finalproject.smartdev.vn.finalproject.adapter.NavDrawerListAdapter;
import finalproject.smartdev.vn.finalproject.constants.Constants;
import finalproject.smartdev.vn.finalproject.infrastructure.Utils;
import finalproject.smartdev.vn.finalproject.model.Bank;
import finalproject.smartdev.vn.finalproject.model.Users;
import finalproject.smartdev.vn.finalproject.webservice.RestClient;
import finalproject.smartdev.vn.finalproject.webservice.RestResponseHandler;


public class MainActivity extends ActionBarActivity implements DrawerLayout.DrawerListener
{

    private static final String TAG = "HomeActivity";
    private boolean isExitApplication = false;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    public static ActionBarDrawerToggle mDrawerToggle;

    // nav drawer title
    private CharSequence mDrawerTitle;

    // used to store app title
    private CharSequence mTitle;

    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;

    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;

    // Model
    private ArrayList<Bank> mBanks = new ArrayList<Bank>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences prefs = getSharedPreferences(Constants.NAME_SHARED_PREFENCES, MODE_PRIVATE);
        String userName = prefs.getString(Constants.USER_NAME, "");
        Log.d("data username : ", userName);

        createActionBar(savedInstanceState);

        if (null != userName && "" != userName) {
            Utils.navigationMapsFragment(getSupportFragmentManager(), null);
        } else {
            Intent intent = new Intent(this, LoginFragment.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG,"7");
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        ProfileFragment profileFragment = (ProfileFragment) getSupportFragmentManager()
                .findFragmentByTag(ProfileFragment.TAG);
        if (profileFragment != null) {
            profileFragment.onBackPress();
            isExitApplication = false;
            return;
        }

        FavoriteFragment favoriteFragment = (FavoriteFragment) getSupportFragmentManager()
                .findFragmentByTag(FavoriteFragment.TAG);
        if (favoriteFragment != null) {
            favoriteFragment.onBackPress();
            isExitApplication = false;
            return;
        }

        MapFragment mapFragment = (MapFragment) getSupportFragmentManager()
                .findFragmentByTag(MapFragment.TAG);
        if (mapFragment != null) {
            mapFragment.onBackPress();
            isExitApplication = false;
            finish();
            return;
        }
    }

    private void createActionBar(Bundle savedInstanceState)
    {
        mTitle = mDrawerTitle = getTitle();
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        navDrawerItems = new ArrayList<NavDrawerItem>();
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));


        // Recycle the typed array
        navMenuIcons.recycle();

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.menu, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

//        if (savedInstanceState == null) {
//            // on first time display view for first nav item
//            displayView(0);
//        }
    }


    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        Log.i(TAG, "1");
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        invalidateOptionsMenu();
        Log.i(TAG,"2");
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        invalidateOptionsMenu();
        Log.i(TAG,"3");
    }

    @Override
    public void onDrawerStateChanged(int newState) {
        Log.i(TAG,"7");
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(mDrawerLayout != null) {
            boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        }
        Log.i(TAG,"4");
        return super.onPrepareOptionsMenu(menu);

    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        Log.i(TAG, "5");
        super.onPostCreate(savedInstanceState);
        if(mDrawerToggle != null){
            mDrawerToggle.syncState();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.i(TAG, "6");
        super.onConfigurationChanged(newConfig);
        if(mDrawerToggle != null) {
            mDrawerToggle.onConfigurationChanged(newConfig);
        }
    }

    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        switch (position) {
            case 0:
                Log.i(TAG,"select navigationProfileFragment");
                getUserProfile();;
                break;
            case 1:
                Log.i(TAG,"select navigationFavoriteFragment");
                Utils.navigationFavoriteFragment(getSupportFragmentManager(),null);
                break;
            case 2:
                Utils.saveStringPreferenceWithKey(getApplicationContext(), Constants.USER_NAME, null);
                Utils.saveStringPreferenceWithKey(getApplicationContext(), Constants.TOKEN, null);

                Intent intent = new Intent(MainActivity.this, LoginFragment.class);
                startActivity(intent);
                finish();
                break;
            case 3:
                finish();
                break;
            default:
                break;
        }
    }

    private void getUserProfile(){

        SharedPreferences prefs = getSharedPreferences(Constants.NAME_SHARED_PREFENCES, MODE_PRIVATE);
        String tokenUser = prefs.getString(Constants.TOKEN, "");

        RestClient.getProfile(MainActivity.this, tokenUser, new RestResponseHandler() {
            @Override
            public void onSuccess(int statusCode, String response) {
                Gson gson = new GsonBuilder().create();
                Users userProfile = gson.fromJson(response, Users.class);

                Bundle bundle = new Bundle();
                bundle.putString("social", userProfile.getIsSocial());
                bundle.putString("firstname", userProfile.getFirstName());
                bundle.putString("lastname", userProfile.getLastName());
                bundle.putString("email", userProfile.getEmail());
                bundle.putString("username", userProfile.getUserName());
                bundle.putString("gender", userProfile.getGender());
                bundle.putString("age", userProfile.getAge());

                Utils.navigationProfileFragment(getSupportFragmentManager(), bundle);
            }

            @Override
            public void onFailure(int statusCode, String response) {
                if(statusCode == 401){
                    Utils.showDialogToken(MainActivity.this, "Failure !!", "Your account has been logged in by another device. Please try to login again !");
                }
            }
        });

    }

    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }
}
