package finalproject.smartdev.vn.finalproject.view;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import finalproject.smartdev.vn.finalproject.R;
import finalproject.smartdev.vn.finalproject.adapter.BankFavoriteAdapter;
import finalproject.smartdev.vn.finalproject.constants.Constants;
import finalproject.smartdev.vn.finalproject.infrastructure.MapUtils;
import finalproject.smartdev.vn.finalproject.infrastructure.Utils;
import finalproject.smartdev.vn.finalproject.model.Bank;
import finalproject.smartdev.vn.finalproject.webservice.RestClient;
import finalproject.smartdev.vn.finalproject.webservice.RestResponseHandler;

public class MapFragment extends Fragment implements View.OnClickListener {
    private AlertDialog mAtmMissingAlert;
    private AlertDialog mGPSAlert;
    private static MapFragment sInstance;
    private GoogleMap mMap;
    public static String TAG = "MapFragment";
    ArrayList<Bank> mBanks = new ArrayList<>();
    private ProgressDialog mProgress;
    private ProgressDialog mDrawPathProgress;
    private LatLng mCurrentLocation;
    BankSearchAdapter mBankAdapter;
    ListView mSearchList;
    private static android.support.v7.widget.SearchView searchView;
    private static MenuItem searchItem;
    HashMap<Marker, Bank> mMarkerBankDictionary = new HashMap<>();
    private Button mButtonATM;
    private Boolean isBanksLoaded = false;
    private Boolean isMapLoaded = false;
    private boolean isLocationUpdated = false;

    public static MapFragment getInstance() {
        if (sInstance == null) {
            sInstance = new MapFragment();
        }
        return sInstance;
    }

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container, false);
        mSearchList = (ListView) v.findViewById(R.id.lst_search_ATM);
        mButtonATM = (Button) v.findViewById(R.id.btn_nearby_bank);
        mButtonATM.setOnClickListener(this);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ensureGPSEnabled();
    }

    private void ensureGPSEnabled() {
        if (!isGPSEnabled()) {
            buildAlertMessageNoGps();
        } else {
            startMainFlow();
        }
    }


    private void startMainFlow() {
        // Get ATMs
        getAllATM();

        // Create progress dialog
        mProgress = new ProgressDialog(getActivity());
        mProgress.setMessage("Loading ATM data...");
        mProgress.setCanceledOnTouchOutside(false);
        mProgress.show();
        mDrawPathProgress = new ProgressDialog(getActivity());
        mDrawPathProgress.setMessage("Drawing direction...");
        mDrawPathProgress.setCancelable(false);

        // Init map & ATMs markers
        if (mMap == null) {
            Fragment frag = getActivity().getSupportFragmentManager().findFragmentByTag(MapFragment.TAG);
            Fragment mapFrag = frag.getChildFragmentManager().findFragmentById(R.id.map);
            mMap = ((SupportMapFragment) mapFrag).getMap();
            if (mMap != null) {
                mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                    @Override
                    public void onMapLoaded() {
                        setUpMap();
                    }
                });
            }
        }
    }

    private void getAllATM() {
        // Get all ATMs
        RestClient.getAllATM(getActivity(), new RestResponseHandler() {
            @Override
            public void onSuccess(int statusCode, String response) {
                Gson gson = new GsonBuilder().create();
                mBanks.addAll(Arrays.asList(gson.fromJson(response, Bank[].class)));
                mBankAdapter = new BankSearchAdapter(getActivity(), mBanks);
                mSearchList.setAdapter(mBankAdapter);
                mSearchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (mBankAdapter != null) {
                            searchItem.collapseActionView();
                            Bank bank = (Bank) mBankAdapter.getItem(position);
                            if (mCurrentLocation != null && bank != null) {
                                mDrawPathProgress.show();
                                MapUtils.drawPath(mCurrentLocation, bank, mMap, mDrawPathProgress, mMarkerBankDictionary, true);
                            }
                        }
                    }
                });
                isBanksLoaded = true;
                if (isMapLoaded && isLocationUpdated) {
                    showNearByATMs();
                    mProgress.dismiss();
                } else {
                    mProgress.setMessage("Loading maps data...");
                }
            }

            @Override
            public void onFailure(int statusCode, String response) {
                mProgress.dismiss();
            }
        });
    }

    private boolean isGPSEnabled() {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101) {
            ensureGPSEnabled();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("This app need to use GPS, please enable it.")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.dismiss();
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 101);
                    }
                });
        mGPSAlert = builder.create();
        mGPSAlert.show();
    }

    private void setUpMap() {
        mMap.setMyLocationEnabled(true);
        setMarkerClickable();
        mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                mCurrentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                if (!isLocationUpdated) {
                    if (isBanksLoaded && isMapLoaded) {
                        showNearByATMs();
                        mProgress.dismiss();
                    } else {
                        mProgress.setMessage("Loading...");
                    }
                    isLocationUpdated = true;
                }
            }
        });
        isMapLoaded = true;
        if (isBanksLoaded && isLocationUpdated) {
            showNearByATMs();
            mProgress.dismiss();
        } else {
            mProgress.setMessage("Loading user location...");
        }
    }

    private void showNearByATMs() {
        if (mBanks != null && mBanks.size() > 0) {
            MapUtils.showATMsOnMap(5000, mCurrentLocation, mBanks, mMap, mMarkerBankDictionary, false);
            // Check whether we find any nearby bank
            if (mMarkerBankDictionary.size() == 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                mAtmMissingAlert = builder.setCancelable(true)
                        .setMessage("Can not find any ATM near your location. Show all ATM instead?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                MapUtils.showATMsOnMap(mCurrentLocation, mBanks, mMap, mMarkerBankDictionary, false);
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                mAtmMissingAlert.show();
            }
        }
    }

    private void setMarkerClickable() {
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                final Bank bank = mMarkerBankDictionary.get(marker);
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                if (bank != null) {
                    builder.setTitle(bank.getTypeBank() + "\n" + bank.getAddressBank());
                }
                CharSequence[] items = new CharSequence[]{"Get direction to this bank", "Add to Favorites", "Share"};
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (bank != null) {
                            if (which == 0) {
                                mDrawPathProgress.show();
                                MapUtils.drawPath(mCurrentLocation, bank, mMap, mDrawPathProgress, mMarkerBankDictionary, true);
                            } else if (which == 1) {
                                SharedPreferences prefs;
                                prefs = getActivity().getSharedPreferences(Constants.NAME_SHARED_PREFENCES, getActivity().MODE_PRIVATE);
                                String tokenUser = prefs.getString(Constants.TOKEN, "");
                                RestClient.addToFavariteBanksOfUser(getActivity().getApplicationContext(), tokenUser, bank.getId(), new RestResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, String response) {
                                        Toast.makeText(getActivity(), "Bank was added to your favorite", Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onFailure(int statusCode, String response) {
                                        if (statusCode == 401) {
                                            Utils.showDialogToken(getActivity(), "Failure !!", "Your account has been logged in by another device. Please try to login again !");
                                        } else {
                                            try {
                                                JSONObject jsonObject = new JSONObject(response);
                                                Toast.makeText(getActivity(), jsonObject.getString("error"), Toast.LENGTH_SHORT).show();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                });
                            } else if (which == 2) {
                                Bundle bundle =new Bundle();
                                bundle.putParcelable(Constants.PARAMETER_BANK,bank);
                                Utils.navigationShareFragment(getActivity().getSupportFragmentManager(),bundle);
                            }
                            dialog.dismiss();
                        }
                    }
                });
                builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                return false;
            }
        });
    }

    public void onBackPress() {
        MapFragment mapFragment = MapFragment.getInstance();
        android.support.v4.app.FragmentTransaction transaction = getActivity()
                .getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.remove(mapFragment);
        transaction.commit();
        sInstance=null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        //super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_favorite, menu);
        searchItem = menu.findItem(R.id.action_search);
        searchView = (android.support.v7.widget.SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                mBankAdapter.getFilter().filter(s);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.i("searchView", searchView.getQuery().toString());
                mBankAdapter.getFilter().filter(s);
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                mSearchList.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                mSearchList.setVisibility(View.VISIBLE);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            /** EDIT **/
            case R.id.action_search:
                return true;


            default:
                return super.onOptionsItemSelected(item);
        }//end switch
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonATM) {
            showNearByATMs();
        }
    }

    public static class BankSearchAdapter extends BankFavoriteAdapter {
        public BankSearchAdapter(Context context, ArrayList<Bank> arrayListBanks) {
            super(context, arrayListBanks);
            this.arrayListBanksFilteredList = new ArrayList<Bank>();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = super.getView(position, convertView, parent);
            v.findViewById(R.id.favariteButton).setVisibility(View.GONE);
            return v;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    // TODO Auto-generated method stub
                    arrayListBanksFilteredList = (ArrayList<Bank>) results.values;
                    notifyDataSetChanged();
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults results = new FilterResults();
                    if (constraint == null || constraint.length() == 0) {
                        results.values = new ArrayList<Bank>();
                        results.count = 0;
                    } else {
                        ArrayList<Bank> filterResult = new ArrayList<Bank>();
                        for (Bank data : arrayListBanksOriginal) {
                            if (data.getTypeBank().toLowerCase()
                                    .contains(constraint.toString().toLowerCase()) || data.getAddressBank().toLowerCase()
                                    .contains(constraint.toString().toLowerCase())) {
                                filterResult.add(data);
                            }
                        }
                        results.values = filterResult;
                        results.count = filterResult.size();
                    }
                    return results;
                }
            };
        }
    }

    @Override
    public void onDetach() {
        sInstance = null;
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        sInstance = null;
        super.onDestroy();
    }
}
