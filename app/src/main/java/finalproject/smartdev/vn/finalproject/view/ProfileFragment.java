package finalproject.smartdev.vn.finalproject.view;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import finalproject.smartdev.vn.finalproject.R;
import finalproject.smartdev.vn.finalproject.constants.Constants;
import finalproject.smartdev.vn.finalproject.infrastructure.Utils;
import finalproject.smartdev.vn.finalproject.model.Users;
import finalproject.smartdev.vn.finalproject.webservice.RestClient;
import finalproject.smartdev.vn.finalproject.webservice.RestResponseHandler;

/**
 * Created by HUNG-HOANG on 4/18/2015.
 */
public class ProfileFragment extends BaseFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    public static final String TAG = "ProfileFragment";
    private static ProfileFragment instance;

    private EditText edtFirstName;
    private EditText edtLastName;
    private EditText edtEmail;
    private EditText edtUsername;
    private Spinner spGender;
    private EditText edtAge;
    private Switch switchMode;
    private EditText edtPassword;
    private EditText edtConfirmPassword;
    private Button btnUpdate;
    private Button btnCancel;
    private LinearLayout layoutSwitchMode;
    private LinearLayout layoutPassword;
    private LinearLayout layoutConfirmPassword;

    private Users userProfile = new Users();
    SharedPreferences prefs;
    String tokenUser;

    public static ProfileFragment getInstance() {
        if (instance == null) {
            instance = new ProfileFragment();
        }
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frament_profile_update,null);
        initViews(view);
        addListenerForViews();

        prefs = getActivity().getSharedPreferences(Constants.NAME_SHARED_PREFENCES, getActivity().MODE_PRIVATE);
        tokenUser = prefs.getString(Constants.TOKEN, "");
        bindDataOnViews();

        return view;
    }

    @Override
    protected void initViews(View view) {
        edtFirstName = (EditText) view.findViewById(R.id.edtFirstname);
        edtLastName = (EditText) view.findViewById(R.id.edtLastname);
        edtEmail = (EditText) view.findViewById(R.id.edtEmail);
        edtUsername = (EditText) view.findViewById(R.id.edtUsername);
        spGender = (Spinner) view.findViewById(R.id.spGender);
        edtAge = (EditText) view.findViewById(R.id.edtAge);
        switchMode = (Switch) view.findViewById(R.id.switchMode);
        edtPassword = (EditText) view.findViewById(R.id.edtPassword);
        edtConfirmPassword = (EditText) view.findViewById(R.id.edtConfirmPassword);
        btnUpdate = (Button) view.findViewById(R.id.btnUpdate);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        layoutSwitchMode = (LinearLayout) view.findViewById(R.id.layoutSwitchMode);
        layoutPassword = (LinearLayout) view.findViewById(R.id.layoutPassword);
        layoutConfirmPassword = (LinearLayout) view.findViewById(R.id.layoutConfirmPassword);
    }

    @Override
    protected void addListenerForViews() {
        switchMode.setOnCheckedChangeListener(this);
        btnUpdate.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        edtAge.setOnClickListener(this);
    }

    @Override
    protected void bindDataOnViews() {
        Bundle bundle = getArguments();

        if (Boolean.parseBoolean(bundle.getString("social"))) {
            layoutSwitchMode.setVisibility(LinearLayout.GONE);
            layoutPassword.setVisibility(LinearLayout.GONE);
            layoutConfirmPassword.setVisibility(LinearLayout.GONE);
        }
        edtFirstName.setText(bundle.getString("firstname"));
        edtLastName.setText(bundle.getString("lastname"));
        edtEmail.setText(bundle.getString("email"));
        edtUsername.setText(bundle.getString("username"));
        String[] mTestArray = getResources().getStringArray(R.array.array_gender);
        spGender.setSelection(Arrays.asList(mTestArray).indexOf(bundle.getString("gender")));
        edtAge.setText(bundle.getString("age"));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.array_gender, R.layout.custom_spinner_item);
        spGender.setAdapter(adapter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnUpdate:
                Users user = new Users();
                user.setToken(tokenUser);
                user.setFirstName(edtFirstName.getText().toString());
                user.setLastName(edtLastName.getText().toString());
                user.setEmail(edtEmail.getText().toString());
                user.setUserName(edtUsername.getText().toString());
                user.setGender(spGender.getSelectedItem().toString());
                user.setAge(edtAge.getText().toString());
                if (switchMode.isChecked()) {
                    user.setCurrentPassword(edtPassword.getText().toString());
                    user.setNewPassword(edtConfirmPassword.getText().toString());
                }
                RestClient.updateAccount(getActivity().getApplicationContext(), user, new RestResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, String response) {
                        if (statusCode == 200) {
                            Toast.makeText(getActivity().getApplicationContext(), "update success", Toast.LENGTH_SHORT)
                              .show();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, String response) {
                        if (statusCode == 401) {
                            Utils.showDialogToken(getActivity(), "Failure !!", "Your account has been logged in by another device. Please try to login again !");
                        }
                    }
                });
                break;
            case R.id.edtAge:
                Utils.setDateTimeField(getActivity(), edtAge);
                break;
            case R.id.btnCancel:
                super.getActivity().onBackPressed();
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            edtPassword.setEnabled(true);
            edtConfirmPassword.setEnabled(true);
        } else {
            edtPassword.setEnabled(false);
            edtConfirmPassword.setEnabled(false);
        }
    }

    @Override
    protected void onBackPress() {
        super.onBackPress();
        instance=null;
    }
}
