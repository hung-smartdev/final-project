package finalproject.smartdev.vn.finalproject.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.SendButton;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.plus.PlusShare;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Arrays;

import finalproject.smartdev.vn.finalproject.R;
import finalproject.smartdev.vn.finalproject.constants.Constants;
import finalproject.smartdev.vn.finalproject.infrastructure.Utils;
import finalproject.smartdev.vn.finalproject.model.Bank;
import twitter4j.GeoLocation;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by hunght on 22/04/2015.
 */
public class ShareFragment extends  BaseFragment implements View.OnClickListener,GraphRequest.GraphJSONObjectCallback{

    public static final String TAG = "ShareFragment";
    private static ShareFragment instance;
    private ImageView imgTwitter;
    private ImageView imgGoogle;
    private ProgressDialog pDialog;
    private Bank bank;
    private ShareButton btnShareFB;
    private CallbackManager callbackManager;
    private ShareDialog shareDialog;
    private static Twitter twitter;
    private static RequestToken requestToken;
    public static final int WEB_VIEW_REQUEST_CODE = 100;


    public static ShareFragment getInstance() {
        if (instance == null) {
            instance = new ShareFragment();
        }
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        View view = inflater.inflate(R.layout.fragment_share,null);
        initViews(view);
        addListenerForViews();
        bindDataOnViews();
        return view;
    }

    @Override
    protected void initViews(View view) {
        imgTwitter = (ImageView)view.findViewById(R.id.imgTwitter);
        imgGoogle = (ImageView)view.findViewById(R.id.imgGoogle);
        btnShareFB = (ShareButton)view.findViewById(R.id.btnShareFB);
        btnShareFB.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        btnShareFB.setShareContent(null);


    }

    @Override
    protected void addListenerForViews() {
        imgTwitter.setOnClickListener(this);
        imgGoogle.setOnClickListener(this);
        btnShareFB.setOnClickListener(this);







    }

    @Override
    protected void bindDataOnViews() {
        Bundle arguments = getArguments();
        if(arguments !=null)
        {
            bank =arguments.getParcelable(Constants.PARAMETER_BANK);
            Log.i(TAG,bank.toString());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imgTwitter:
                boolean isLoginIn = Utils.getBooleanPreferenceWithKey(getActivity().getApplicationContext(), Constants.PREF_KEY_TWITTER_LOGIN, false);
                if(isLoginIn) {
                    showDialogTwitter();
                }
                else{
                    loginTwitter();

                }

                break;
            case R.id.imgGoogle:
                shareGoogle(bank);
                break;
            case R.id.btnShareFB:

                Log.i(TAG,"Share facebook"+AccessToken.getCurrentAccessToken());
                shareFacebook(bank);
                break;

        }
    }

    private void showDialogTwitter()
    {
        LayoutInflater li = LayoutInflater.from(getActivity());
        View twitterPost = li.inflate(R.layout.dialog_twitter_post_status, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        alertDialogBuilder.setView(twitterPost);
        alertDialogBuilder.setIcon(R.drawable.share_twitter);
        alertDialogBuilder.setTitle("Twitter");
        final EditText userInput = (EditText) twitterPost
                .findViewById(R.id.editContent);
        userInput.setText(bank.getTypeBank() + "\n" + bank.getAddressBank() + "\n\n" + Constants.URL_MAP_POSITION + bank.getLatBank() + "," + bank.getLngBank());
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                shareTwitter(userInput.getText().toString(), null);

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {

    }

    public void shareGoogle(Bank bank)
    {
        Intent shareIntent = new PlusShare.Builder(getActivity())
                .setType("text/plain")
                .setText(bank.getTypeBank()+"\n"+bank.getAddressBank())
                .setContentUrl(Uri.parse(Constants.URL_MAP_POSITION + bank.getLatBank() + "," + bank.getLngBank()))
                .getIntent();
        startActivityForResult(shareIntent, 0);
    }

    public void shareFacebook(Bank bank)
    {
        Log.i(TAG,"Share facebook OK 1");
        final ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(Constants.URL_MAP_POSITION+bank.getLatBank()+","+bank.getLngBank()))
                .setContentTitle(bank.getTypeBank())
                .setContentDescription(bank.getTypeBank()+"\n"+bank.getAddressBank())
                .build();
        btnShareFB.setShareContent(content);

        ShareApi.share(content, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.i(TAG,"Share facebook onSuccess");
            }

            @Override
            public void onCancel() {

                Log.i(TAG,"Share facebook onCancel");

            }

            @Override
            public void onError(FacebookException e) {
                Log.i(TAG,"Share facebook onError");


            }
        });


    }

    public void shareFacebookGraphRequest(){

        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("message","boooo");
            jsonObject.put("description","booo5555555555o");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        GraphRequest request= GraphRequest.newPostRequest(AccessToken.getCurrentAccessToken(),"me/feed",null,new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                Log.i(TAG,"Share facebook OK"+graphResponse);
            }
        });
        Bundle postParams = request.getParameters();
        postParams.putString("caption", "my picture");
        postParams.putString("message", "ABC");
        postParams.putString("description", "test test test");
        request.setParameters(postParams);
        request.executeAsync();
    }

    public void shareTwitter(String title, GeoLocation geoLocation)
    {
        ShareStatusTwitter shareStatusTwitter =new ShareStatusTwitter();
        shareStatusTwitter.execute(title,geoLocation);

    }

    private class ShareStatusTwitter extends AsyncTask<Object, String, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Posting to twitter...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        @Override
        protected Void doInBackground(Object... params) {
            String status = params[0].toString();
            try {

                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setOAuthConsumerKey(Constants.TWITTER_CONSUMER_KEY);
                builder.setOAuthConsumerSecret(Constants.TWITTER_CONSUMER_SECRET_KEY);

                String access_token = Utils.getStringPreferenceWithKey(getActivity(),Constants.PREF_TWITTER_OAUTH_KEY,"");
                String access_token_secret = Utils.getStringPreferenceWithKey(getActivity(),Constants.PREF_TWITTER_OAUTH_KEY_SECRET,"");
                twitter4j.auth.AccessToken accessToken = new twitter4j.auth.AccessToken(access_token, access_token_secret);
                Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

                StatusUpdate statusUpdate = new StatusUpdate(status);
                statusUpdate.setLocation((GeoLocation)params[1]);
                twitter4j.Status response = twitter.updateStatus(statusUpdate);

                Log.d("Status", response.getText());

            } catch (TwitterException e) {
                Log.d("Failed to post!", e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            pDialog.dismiss();
            Toast.makeText(getActivity(), "Posted to Twitter!", Toast.LENGTH_SHORT).show();


        }
    }

    private void loginTwitter()
    {
        boolean isLoginIn = Utils.getBooleanPreferenceWithKey(getActivity().getApplicationContext(), Constants.PREF_KEY_TWITTER_LOGIN, false);
        if( !isLoginIn )
        {
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(Constants.TWITTER_CONSUMER_KEY);
            builder.setOAuthConsumerSecret(Constants.TWITTER_CONSUMER_SECRET_KEY);
            Configuration configuration = builder.build();
            TwitterFactory factory = new TwitterFactory(configuration);
            twitter = factory.getInstance();
            try {
                requestToken = twitter.getOAuthRequestToken(Constants.TWITTER_CALLBACK);
                Intent intent = new Intent(getActivity(), WebViewActivity.class);
                intent.putExtra(WebViewActivity.EXTRA_URL, requestToken.getAuthenticationURL());
                startActivityForResult(intent, WEB_VIEW_REQUEST_CODE);

            } catch (TwitterException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            String verifier = data.getExtras().getString(Constants.TWITTER_OAUTH_VERIFIER);
            try {
                if (twitter != null) {
                    twitter4j.auth.AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, verifier);
                    Utils.saveBooleanPreferenceWithKey(getActivity(),Constants.PREF_KEY_TWITTER_LOGIN,true);
                    Utils.saveStringPreferenceWithKey(getActivity(),Constants.PREF_TWITTER_OAUTH_KEY,accessToken.getToken());
                    Utils.saveStringPreferenceWithKey(getActivity(),Constants.PREF_TWITTER_OAUTH_KEY_SECRET,accessToken.getTokenSecret());
                    showDialogTwitter();
                }
            } catch (TwitterException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    protected void onBackPress() {
        super.onBackPress();
        instance=null;
    }
}
