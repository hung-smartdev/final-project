package finalproject.smartdev.vn.finalproject.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import finalproject.smartdev.vn.finalproject.R;
import finalproject.smartdev.vn.finalproject.constants.Constants;

/**
 * Created by HUNG-HOANG on 4/20/2015.
 */
public class WebViewActivity extends Activity{

    private WebView webView;
    public static String EXTRA_URL = "extra_url";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_web_view);
        final String url = this.getIntent().getStringExtra(EXTRA_URL);
        if (null == url) {
            Log.e("Twitter", "URL cannot be null");
            finish();
        }
        Log.e("Twitter", "URL: "+url);

        webView = (WebView) findViewById(R.id.webView);
        webView.setWebViewClient(new MyWebViewClient());
        webView.loadUrl(url);
        webView.loadUrl(url);
    }

    class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            if(url.startsWith(Constants.TWITTER_URL_LOGOUT))
            {
                Log.i("Twitter", "URL LOGOUT");
                finish();
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if (url.contains(Constants.TWITTER_CALLBACK)) {
                Uri uri = Uri.parse(url);
                Log.i("Twitter", "shouldOverrideUrlLoading");
                String verifier = uri.getQueryParameter(Constants.TWITTER_OAUTH_VERIFIER);
                Intent resultIntent = new Intent();
                resultIntent.putExtra(Constants.TWITTER_OAUTH_VERIFIER, verifier);
                setResult(RESULT_OK, resultIntent);
                finish();
                return true;
            }
            return false;
        }


    }


}
