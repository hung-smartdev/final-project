package finalproject.smartdev.vn.finalproject.webservice;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

/**
 * Created by Hoang on 4/20/2015.
 */
public class BaseHttpResponseHandler extends AsyncHttpResponseHandler {
    private Context mContext;
    public BaseHttpResponseHandler(Context context) {
        mContext = context;
    }
    @Override
    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
        Log.d("Base handler", "Status code: " + statusCode);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
        Log.d("BaseHttpResponse", "Status code:" + statusCode);
        Toast.makeText(mContext, "Can not connect to server!", Toast.LENGTH_LONG);
    }
}
