package finalproject.smartdev.vn.finalproject.webservice;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import finalproject.smartdev.vn.finalproject.constants.Constants;
import finalproject.smartdev.vn.finalproject.infrastructure.Utils;
import finalproject.smartdev.vn.finalproject.model.Bank;
import finalproject.smartdev.vn.finalproject.model.Users;

/**
 * Created by Hoang on 4/20/2015.
 */
public class RestClient {
    private static final String BASE_URL = "http://107.191.107.93:1337/";
    private static AsyncHttpClient client = new AsyncHttpClient();

    // BASE COMPONENT
    public static void get(String url, RequestParams params, BaseHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, BaseHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void put(String url, RequestParams params, BaseHttpResponseHandler responseHandler) {
        client.put(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

    // ADVANCE
    public static void getAllATM(Context context, final RestResponseHandler handler) {
        get("bank", null, new BaseHttpResponseHandler(context) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    handler.onSuccess(statusCode, new String(response, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                super.onFailure(statusCode, headers, responseBody, error);
                try {
                    handler.onFailure(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        });
    }

    public static void getAllFavariteBanksOfUser(Context context, String tokenUser, final RestResponseHandler handler) {
        get("bank/" + tokenUser + "/favorite", null, new BaseHttpResponseHandler(context) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                try {
                    handler.onSuccess(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                super.onFailure(statusCode, headers, responseBody, error);
                try {
                    handler.onFailure(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        });
    }

    public static void addToFavariteBanksOfUser(Context context, String tokenUser, String bankID, final RestResponseHandler handler) {
        RequestParams params = new RequestParams();
        params.put("bankId", bankID);
        post("bank/" + tokenUser + "/favorite", params, new BaseHttpResponseHandler(context) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                try {
                    handler.onSuccess(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                super.onFailure(statusCode, headers, responseBody, error);
                try {
                    handler.onFailure(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        });
    }

    public static void removeBankFromFavariteBanksOfUser(Context context, String tokenUser, String bankID, final RestResponseHandler handler) {
        RequestParams params = new RequestParams();
        params.put("bankId", bankID);
        put("bank/" + tokenUser + "/favorite", params, new BaseHttpResponseHandler(context) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                try {
                    handler.onSuccess(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                super.onFailure(statusCode, headers, responseBody, error);
                try {
                    handler.onFailure(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        });
    }

    public static void createNewAccount(Context context, Users users, final RestResponseHandler handler) {
        post("user", toPostCreateAccount(users), new BaseHttpResponseHandler(context) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                try {
                    handler.onSuccess(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                super.onFailure(statusCode, headers, responseBody, error);
                try {
                    handler.onFailure(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        });
    }

    public static void createNewAccountSocial(Context context, Users users, final RestResponseHandler handler) {
        post("user/social", toPostCreateAccount(users), new BaseHttpResponseHandler(context) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                try {
                    handler.onSuccess(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                super.onFailure(statusCode, headers, responseBody, error);
                try {
                    handler.onFailure(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        });
    }


    public static void updateAccount(Context context, Users user, final RestResponseHandler handler) {
        put("user/" + user.getToken(), toPutUpdateAccount(user), new BaseHttpResponseHandler(context) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                try {
                    handler.onSuccess(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                super.onFailure(statusCode, headers, responseBody, error);
                try {
                    handler.onFailure(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        });
    }

    public static void login(Context context, Users users, final RestResponseHandler handler) {
        post("user/login", toPutLogin(users), new BaseHttpResponseHandler(context) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                try {
                    handler.onSuccess(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                super.onFailure(statusCode, headers, responseBody, error);
                try {
                    handler.onFailure(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        });
    }

    public static void getProfile(Context context, String tokenUser, final RestResponseHandler handler) {
        get("user/" + tokenUser, null, new BaseHttpResponseHandler(context) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                try {
                    handler.onSuccess(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                super.onFailure(statusCode, headers, responseBody, error);
                try {
                    handler.onFailure(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        });
    }

    public static void forgotPassword(Context context, String email, final RestResponseHandler handler) {
        get("reset/" + email, null, new BaseHttpResponseHandler(context) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                super.onSuccess(statusCode, headers, responseBody);
                try {
                    handler.onSuccess(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                super.onFailure(statusCode, headers, responseBody, error);
                try {
                    handler.onFailure(statusCode, new String(responseBody, "UTF-8"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ;
            }
        });
    }

    public static RequestParams toPostCreateAccount(Users users) {
        RequestParams params = null;
        params = new RequestParams();
        params.put("firstName", users.getFirstName());
        params.put("lastName", users.getLastName());
        params.put("userName", users.getUserName());
        params.put("email", users.getEmail());
        params.put("gender", users.getGender());
        params.put("age", String.valueOf(users.getAge()));
        params.put("password", users.getPassword());
        params.put("token", users.getToken());
        params.put("isSocial", users.getIsSocial());
        return params;
    }

    public static RequestParams toPutUpdateAccount(Users users) {
        RequestParams params = null;
        params = new RequestParams();
        params.put("firstName", users.getFirstName());
        params.put("lastName", users.getLastName());
        params.put("userName", users.getUserName());
        params.put("email", users.getEmail());
        params.put("gender", users.getGender());
        params.put("age", users.getAge());


        if (users.getNewPassword() != null && users.getCurrentPassword() != null) {
            params.put("currentPassword", users.getCurrentPassword());
            params.put("newPassword", users.getNewPassword());
        }

        return params;
    }

    public static RequestParams toPutLogin(Users users) {
        RequestParams params = null;
        params = new RequestParams();
        params.put("userName", users.getUserName());
        params.put("password", users.getPassword());

        return params;
    }


}
