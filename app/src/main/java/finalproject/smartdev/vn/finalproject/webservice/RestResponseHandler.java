package finalproject.smartdev.vn.finalproject.webservice;

/**
 * Created by Hoang on 4/20/2015.
 */
public interface RestResponseHandler {
    public void onSuccess(int statusCode, String response);
    public void onFailure(int statusCode, String response);
}
